import { Injectable } from '@nestjs/common';
import { Rights } from './users/right';

@Injectable()
export class User {
  id: number|null;
  username: string;
  firstName: string;
  lastName: string;
  password: string;
  creationTime: Date | null;
  public rights: Rights;

  constructor(id:number, firstname: string, lastname: string, username: string, passwort: string){
    this.id = id;
    this.username = username;
    this.firstName = firstname;
    this.lastName = lastname;
    this.password = passwort;
    this.creationTime = new Date;
    this.rights = 0;
  }
}