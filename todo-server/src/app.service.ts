import { Injectable } from '@nestjs/common';
import { filter } from 'rxjs';
import { User } from './User';

@Injectable()
export class AppService {
  
  userlist = [
    new User(1,"KP", "Kevin", "Piam", "kevin"),
    new User(2, "WT",  "Winnie", "Tongle", "winnie"),
    new User(3, "MT", "Maxwell", "Tchiabe", "maxwell")
  ]

  getHello(): string {
    return 'Hello World!';
  }

  getTodoItem(): Array<User> {
    return this.userlist
  }
  addNewUser(user: User): string{
    if(user.firstName!= null 
      && user.lastName != null 
      && user.username != null 
      && user.password != null
      && user.rights != null)
    this.userlist.push(user)
    else return "error"
  } 

  updateUser(userN: User) {
    for(let i = 0 ; i < this.userlist.length; i++){
      if(this.userlist[i].id == userN.id){
        this.userlist[i] = userN;
      }
    }
  }

  deleteUser(id: number){
    this.userlist = this.userlist.filter(user => user.id != id)
  }
}
