import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Observable } from 'rxjs';
import { Reflector } from '@nestjs/core';
import { Rights } from 'src/users/right';

@Injectable()
export class RolesGuard implements CanActivate {
    constructor(private reflector: Reflector) { }
    
    canActivate(context: ExecutionContext): boolean {
        const requiredRole = this.reflector.get<Rights>(
            'role', context.getHandler());
        const request = context.switchToHttp().getRequest();
        return request.session.user.rights >= requiredRole;
    }
}