import express = require('express');
import { Body, Controller, Delete, Get, Param, Post, Put, Req } from '@nestjs/common';
import { Request } from 'express';
import { AppService } from './app.service';
import { User } from './User';
import * as session from 'express-session';
import { Session, BadRequestException, UnauthorizedException } from '@nestjs/common';
import { RolesGuard } from './Roles/guard';
import { Rights } from './users/right';
import { SetMetadata } from '@nestjs/common';
import { UseGuards } from '@nestjs/common';
import { ISession } from './Authentification/ISession';
import { IsLoggedInGuard } from './Authentification/guard';
import { SqlService } from './SqlService';


@Controller('user')
@UseGuards(IsLoggedInGuard, RolesGuard)
export class AppController {

  constructor(
    private readonly appService: AppService,
    private sqlService: SqlService
  ) { }

  @Get()
  @SetMetadata('role', Rights.User)
  async findAllUsers(@Req() request: Request): Promise<any> {
    let userList;
    try {
       userList = await this.sqlService.findAllUser()
      console.log("userList: ", userList)
  } catch(e) {
    console.error(e);
    console.log ("error by findAllUser: ", e)
    throw new UnauthorizedException();
  }
  return userList;
  }

  @Post()
  @SetMetadata('role', Rights.Admin)
  async addNewUser(@Body() user: any): Promise<{ message: string }> {
    if (user == null) {
      throw new BadRequestException();
    }
    try {
    console.log("request (Add User). ", user)
    const newList = await this.sqlService.addNewUser(user);
    console.log("newList by addUser: ", newList)
  } catch(e) {
    console.error(e);
    throw new UnauthorizedException();
  }
  return {message: " Updeate succesfull !"}
  }

  @Put(':id')
  @SetMetadata('role', Rights.Admin)
  async updateUser(@Body() user: User): Promise<{ message: string }>{
    if (user == null) {
      throw new BadRequestException();
    }
    try {
      console.log("try to update user: ", user)
    const newList = await this.sqlService.updateUserById(user)
  } catch(e) {
    console.error("error by udate: ", e);
    throw new UnauthorizedException();
  }
  return {message: " Updeate succesfull !"}
  }

  @Delete(':id')
  @SetMetadata('role', Rights.Admin)
  async deleteUser(@Param('id') id: number): Promise<{ message: string }> {
    if (id == null) {
      throw new BadRequestException();
    }
    try {
      console.log("try to delete user :", id)
    const restList = await this.sqlService.deleteUserById(id)
  } catch(e) {
    console.error("error by delete: ", e);
    throw new UnauthorizedException();
  }
  return {message: " Delete succesfull !"}
}
  
}

@Controller('session')
export class AppSession {

  constructor(
    private readonly appService: AppService,
    private sqlService: SqlService,
  ) { }

  @Get()
  find(@Req() request: Request): string {
    console.log("request get")
    return 'This action returns the session';
  }

  @Post("login")
  async login(
    @Body("username") username: string | null,
    @Body("password") password: string | null,
    @Session() session: ISession,
  ): Promise<{ message: string }> {
    if (username == null || password == null) {
      throw new BadRequestException();
    }
    try {
      const loggedInUser = await this.sqlService.getUserByUsernameAndPassword(username, password);
      session.user = loggedInUser;
    } catch(e) {
      console.error(e);
      throw new UnauthorizedException();
    }
    return {
      message: "You are logged in",
    }
  }

  @Post('logout')
  logOut(@Req() request: Request) {
    IsLoggedInGuard.deleteSession(request);
    return true;
  }

  @Get('check')
  findsession(@Req() request: Request) {
    IsLoggedInGuard.applyLastSession(request);
    return !!request.session['user'];
  }
}

