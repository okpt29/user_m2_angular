import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Observable } from 'rxjs';
import { Reflector } from '@nestjs/core';

@Injectable()
export class IsLoggedInGuard implements CanActivate {
    constructor(private reflector: Reflector) { }

    static applyLastSession(request: any) {
        const key = Object.keys(request.sessionStore.sessions)?.[0];
        const session = request.sessionStore.sessions[key];
        if (!session) {
            return;
        }
        request.session.user = JSON.parse(session)?.user;
        delete request.sessionStore.sessions[key];
    }

    static deleteSession(request: any) {
        const key = Object.keys(request.sessionStore.sessions)?.[0];
        const session = request.sessionStore.sessions[key];
        if (!session) {
            return;
        }
        delete request.sessionStore.sessions[key];
    }
    
    canActivate(context: ExecutionContext): boolean {
        const request = context.switchToHttp().getRequest();
        IsLoggedInGuard.applyLastSession(request);
        return !!request.session.user;
    }
}