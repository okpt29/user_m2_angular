import { User } from "src/User";
export interface ISession {
    user: User | null;
  }