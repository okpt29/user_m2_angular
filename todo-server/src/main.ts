import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as session from 'express-session';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });
  app.use(
    session({
    secret: 'wbs2',
    resave: false,
    saveUninitialized: false,
    }),
    );
  await app.listen(3000);
  console.log('Server started: http://localhost:3000');
}
bootstrap();
