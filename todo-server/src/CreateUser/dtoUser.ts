import { Rights } from "src/users/right";
export class User {
    id: number|null;
    username: string;
    firstName: string;
    lastName: string;
    passwort: string;
    creationTime: Date | null;
   rights: Rights
}