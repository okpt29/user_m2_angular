import { Injectable } from "@nestjs/common";
import { Connection, createConnection, MysqlError } from 'mysql';
import { Observable } from "rxjs";
import { User } from "./User";
import { Rights } from "./users/right";

@Injectable()
export class SqlService {
    private database: Connection;
    constructor() {
        this.database = createConnection({
            user: "root",
            password: "",
            port: 3306,
            database: "userman_nest"
        });
        this.database.connect((error: MysqlError | null) => {
            if (error) { console.error(error) }
        });
    }

    private query<T>(creator: ((rows: any[]) => T), query: string, data?: any[]): Promise<T> {
        return new Promise((resolve, reject) => {
            this.database.query(query, data, (err: MysqlError | null, rows: any) => {
                if (err != null) {
                    reject(err);
                } else {
                    resolve(creator(rows));
                }
            });
        })
    }

    private convertToUser(row: any): User {
        if (!row) {
            return null;
        }
        const user = new User(
            row.id,
            row.firstName,
            row.lastName,
            row.username,
            row.password
        );
        user.creationTime = new Date(row.creationTime);
        user.rights = row.rights;
        return user;
    }

    public getUserByUsernameAndPassword(username: string, password: string): Promise<User> {
        return this.query((rows) => this.convertToUser(rows?.[0]),
         'SELECT * FROM `userlist` WHERE (`username` = ?) AND (`password` = ?);',  [username, password]);
    }

    public deleteUserById(id: number): Promise<any>{
        return this.query((rows) =>{
            return rows
        }, 'DELETE FROM `userlist` WHERE (`id`= ?)', [id]);
    }

    public updateUserById(user: User): Promise<any>{
        let data: [string, string, string, string, Rights, number] = [user.firstName, user.lastName, user.username, user.password, user.rights, user.id];
        console.log("Data for update : ", data)
        return this.query((rows) =>{
            return rows
        }, 'UPDATE `userlist` SET firstName = ?, lastName = ?, username = ?, password = ?, rights = ? WHERE (`id`= ?);', data);
    }

    public addNewUser(user: User): Promise<any>{
        let data: [Date, string, string, string, string, Rights] = [new Date(), user.username, user.password, user.firstName, user.lastName, user.rights || 0];
        return this.query((rows) =>{
            return rows
        }, 'INSERT INTO userlist (creationTime, username, password, firstName, lastName, rights) VALUES (?, ?, ?, ?, ?, ?);', data);
    }

    public findAllUser(): Promise<User[]>{
        return this.query(rows => rows.map(row => this.convertToUser(row)), 'SELECT * FROM userlist');
    }
}