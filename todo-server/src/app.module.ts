import { Global, Module } from '@nestjs/common';
import { AppController, AppSession } from './app.controller';
import { AppService } from './app.service';
import { SqlService } from './SqlService';
import { ServeStaticOptions } from '@nestjs/platform-express/interfaces/serve-static-options.interface';
import { ServeStaticModule } from '@nestjs/serve-static';

@Global()
@Module({
  imports: [ 
    ServeStaticModule.forRoot({
    rootPath: `${__dirname}/../../../client/dist/client`,
    }),
  ],
  controllers: [AppController, AppSession],
  providers: [AppService, SqlService],
})
export class AppModule {}
